# HyCSim: A rapid design space exploration tool for emerging hybrid last-level caches

## Overview

HyCSim is a trace-driven simulation infrastructure that enables rapid comparison of various hybrid LLC configurations for different optimization objectives. It models the full functionality of a hybrid LLC (SRAM-NVM) in order to evaluate different content management mechanisms and compare them in terms of performance (hit rate) and number of writes to the NVM part. Further details can be found in the original paper [1].

## Fault map

In the current version of the simulator the fault-map file is assumed to have as many lines as the number of frames in the hybrid LLC.

Every line is the number of non-faulty bytes each cache frame has. The expected structure is as follows. First, the alive bytes of NVM frames from set 0 are followed by the alive bytes of SRAM frames of set 0. Then, alive bytes from set 1 and so on.

`DisablingManager.py` module can create two simple fault maps:
1. `$> python DisablingManager.py --alive --size=<size>MB` to create a non-faulty byte map.
2. `$> python DisablingManager.py --nvm-dead --size=<size>MB` to create a byte map with all the NVM bytes faulty.

Different fault maps can be created using external tools.

## Running HyCSim

To run HyCSim we should specify the hybrid LLC model we want to simulate, the name of the trace file (without .txt extension) and the fault map.

```
$> python HyCSim.py -v <model> -t <trace> -f <fault_map> 
```

### Example
```
$> python DisablingManager.py --alive --size=16MB
$> python HyCSim.py -v BaseHybrid -t example_trace -f ABA_16MB 
```

## References

Please cite the following article if you use this repository:

[1] Carlos Escuin, Asif Ali Khan, Pablo Ibañez, Teresa Monreal, Victor Viñs and Jeronimo Castrillon. 2022. HyCSim: A rapid design space exploration tool for emerging hybrid last-level caches. In System Engineering for constrained embedded systems (DroneSE and RAPIDO) June 21, 2022, Budapest, Hungary. ACM, New York, NY, USA, 6 pages. https://doi.org/10.1145/3522784.3522801
